package co.njinx.f5begone;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.option.Perspective;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFW;

public class F5BegoneClient implements ClientModInitializer {
    private static KeyBinding thirdPersonBack;
    private static KeyBinding thirdPersonFront;

    @Override
    public void onInitializeClient() {
        thirdPersonBack = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                "key.f5begone.back",
                InputUtil.Type.KEYSYM,
                GLFW.GLFW_KEY_R,
                "category.f5begone"
        ));
        thirdPersonFront = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                "key.f5begone.front",
                InputUtil.Type.KEYSYM,
                GLFW.GLFW_KEY_G,
                "category.f5begone"
        ));

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            while (thirdPersonBack.wasPressed()) {
                if (MinecraftClient.getInstance().options.getPerspective().isFrontView()
                        || MinecraftClient.getInstance().options.getPerspective().isFirstPerson()) {
                    MinecraftClient.getInstance().options.setPerspective(Perspective.THIRD_PERSON_BACK);
                } else {
                    MinecraftClient.getInstance().options.setPerspective(Perspective.FIRST_PERSON);
                }
            }

            while (thirdPersonFront.wasPressed()) {
                if (MinecraftClient.getInstance().options.getPerspective().isFrontView()) {
                    MinecraftClient.getInstance().options.setPerspective(Perspective.FIRST_PERSON);
                } else {
                    MinecraftClient.getInstance().options.setPerspective(Perspective.THIRD_PERSON_FRONT);
                }
            }
        });
    }
}
